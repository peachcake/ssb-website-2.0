# ssb-website-2.0

This is a collaborative repo for the redesign and building of [ssb.nz](ssb.nz)

## Get started

Git Clone

```bash
cd ssb-website-2.0
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

## Rough Design

We have a rough design here on [Figma](https://www.figma.com/file/cncbZdFKTpOEbKYGPjkTczVc/scuttlebutt?node-id=0%3A1).
There is a ongoing discussion & collaboration thread on SSB too! It's here `%90cpJesddE8aullfGxLR70nCRWE7i2F1gkuwZNujRnc=.sha256`.


## Contributing

### Fork, Code and Merge Request

We recommend  to pair program with a friendly scuttlebutter and/or try build out a section yourself!

We're using [svelte](https://svelte.dev/) to allow us to [scope styles](https://svelte.dev/tutorial/styling), and have flexibility in collaboratively making styles indepently for each section / component.

> *... rules are scoped to the component. You won't accidentally change the style of elements elsewhere in your app.*

Mostly svelte just is great because it allows us to work with those who are just starting out learning HTML / CSS,  as a microfrontend framework, and allows different sections of our site to be built without crazy (merge) conflicts of styles.

### To Build:

Please claim a section to work on, and edit this list as pages are completed as you go!

**Site Map**

* [x] Main home page setup and svelte sections ~ [mnin](https://gitlab.com/marylychee)

* [x] Splash ~ [mnin](https://gitlab.com/marylychee)
    * [x] download page ~ [mnin](https://gitlab.com/marylychee)
* [x] Values: Quickly mentions 4 values, Button “read more” which takes us to another page that explains all these ~ mnin
    * [x] Diversity/intersectionality
    * [x] Local-first
    * [x] Offline usable
    * [x] Open sourced
    * [ ] Link to the SSB Principles Stack
* [ ] Decentralised mini explainer banner ~ corlock
    * [ ] Short explanation of decentralisation
    * [ ] Link to the Protocol Guide
    * [ ] Link to Research & Publications
        * [ ] Research & Publications Page
* [x] Proof we legit
    * [x] Videos ~ polylith
    * [x] Fun quote ~ endo
* [x] For contributors ~ 🌔 evening crew (polylith)
* [x] Mapping the ecosystem ~ 🌔 evening crew (endo)


### Decentralised Build Record

Take a note of approx. time you've spent on things. We plan to reimburse people through the Open Collective. :D

Right now some of this is in the airtable spreadsheet. If you don't have access to it or prefer to use keep record here, please do so!


| Section/Task                                                          | Whom                  | Date      | Time       |
|-----------------------------------------------------------------------|-----------------------|-----------|------------|
| Week One - Project Organising                                         | mnin, endo             | 10/6/19 | 3:00:00    |
| Week Two - Project Organising                                         | mnin                  | 17/6/19 | 1:00:00    |
| Design site map / General project setup                               | mnin, Angelica Blevins | 20/6/19 | 1:00:00    |
| Pair project set up                                                   | mnin, Angelica Blevins | 20/6/19 | 11:00      |
| Pair program with Angelica                                            | mnin, Angelica Blevins | 22/6/19 | 1:11:00    |
| Continue to break up site into codeable sections                      | mnin                  | 23/6/19 | 2:30:00    |
| Week Three - Project Organising                                       | mnin, endo             | 25/6/19 | 3:00:00    |
| admin/dev setup                                                       | endo                  | 27/6/19   | 1:09:00    |
| made Not Match / 404 page - including styles                          | mnin                  | 28/6/19 | 1:00:00    |
| Update to Page Not Found                                              | mnin                  | 29/6/19 | 15:00      |
| Post on Scuttle Butt update on things, and also push a little of code | mnin                  | 30/6/19 | 1:30:00    |
| Comms on Scuttle Butt                                                 | mnin                  | 2/7/19  | 1:00:00    |
| admin/comms                                                           | endo                  | 2/7/19    | 21:00      |
| Code and comms on SSB                                                 | mnin, endo             | 3/7/19  | 20:00      |
| Admin and comms on ssb                                                | mnin                  | 3/7/19    |   33:00 |
| Main Splash Page                       | mnin          | 3/7/19    |   60:00 |
| troubleshooting svelte                 | endo, polylith| 3/7/19    |   60:00 |
| documentation/admin                    | endo          | 3/7/19    |   40:00 |
| svelte troubleshooting                 | endo, polylith| 4/7/19	   | 1:00:00 |
| coded download page                    | mnin          | 4/7/19    |   60:00 |
| coded values section                   | mnin          | 4/7/19    |   35:00 |
| started code on proof we legit section | mnin          | 4/7/19    |   22:00 |
| call & admin                           | mnin, endo    | 6/7/19    |   45:00 |
| cleaning up build records              | mnin          | 6/7/19    |   45:00 |
| values - make a cute see more button and style clean up   | mnin          | 7/7/19    |   45:00 |
| pull merges and reviewing Accessibility CSS   | mnin, Sam          | 8/7/19    |   20:00 |
| code & comms up to today               | polylith      | 6/9/19    |   8:47:00 |
| Launch 🎉 🎊 🙌 	                      |               | 11/7/19   |         |


## Miscellaneous References

* [localization support](https://github.com/stefalda/localized-strings) for
  polish?
